package MySqlDAO;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import Model.User;

public class UserInfoDAO extends jdbc{
	
	User user;
	
    public UserInfoDAO() {
		this.connection();
	}
    
	public User get(String username) {
		PreparedStatement stmt = null;
		try
		{
			
			String sql = "select * from user_info where user_account = ?";
			//System.out.println("���ӳɹ�");
			stmt = conn.prepareStatement(sql);
			//System.out.println("׼������");
			stmt.setString(1, username);
			res = stmt.executeQuery();
			//System.out.println("sql���ִ�гɹ�");
			
			if(res.next())
			{
				
				user = new User();
				user.setUseraccount(res.getString(1));
				user.setPwd(res.getString(2));
				user.setPhone(res.getString(3));
				user.setEmail(res.getString(4));
				user.setIdentity(res.getInt(1));
				user.setStatus(res.getInt(2));	
			}
			else
				user = null;
		}catch (SQLException e) {

            e.printStackTrace();
		}
		return user;
	}

}
