package com.c2c;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Controller.Controller;
import Model.User;

/**
 * Servlet implementation class RegisterServlet
 */
@WebServlet(name = "registerServlet", urlPatterns = { "/registerServlet" })
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegisterServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

    protected void service(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
 
    	//获取用户的注册信息
    	String username = request.getParameter("username");
		String password = request.getParameter("password");
		String phonenum = request.getParameter("phoneNum"); 
		String emailnum = request.getParameter("emailNum"); 
		int userIdentity =  Integer.parseInt(request.getParameter("userIdentity"));
		int registerStatus = 0;
		String invitenum = request.getParameter("inviteNum");
		
		User user = new User();
		user.setUsername(username);
		user.setPassword(password);
		user.setPhonenum(phonenum);
		user.setEmailnum(emailnum);
		user.setUserIdentity(userIdentity);
		user.setRegisterStatus(registerStatus);
		
		// 调用控制器的注册方法
		Controller con = new Controller();
		
		//验证通过
		if(con.register(user,invitenum)) {
			//网页跳转
		}else {
			//提示用户名重复
		}
 
    }

}
